﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonBehaviour<GameManager> {

    [SerializeField] private GameObject player;

    private static class PermanentData {
        public static int currentLevel = 0;
    }

    public void WinGame () {
        PermanentData.currentLevel++;
        SceneManager.LoadScene(PermanentData.currentLevel);
    }

    private void StartLevel (string levelName) {
        SceneManager.LoadScene(levelName);
    }

    public static GameObject GetPlayer () {
        return Instance.player;
    }

    public static Inventory GetInventory () {
        return Instance.GetComponent<Inventory>();
    }
}
