﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class RenderingEffects : SingletonBehaviour<RenderingEffects>
{
    [SerializeField] private Material Desaturation;
    [SerializeField] private Material Displacement;
    [Range(0, 0.01f)] public float displacementMin = 0;
    [Range(0, 1)] public float desaturationMin = 0;
    [Range(0, 2)] public float DesaturationRange = 1;
    [Range(0, 0.05f)] public float DisplacementRange = 0.015f;

    public bool DesaturationON = true;
    public bool DisplacementON = true;
    public float DesaturationValue
    {
        set
        {
            value = value > DesaturationRange ? DesaturationRange : value;
            value = value < desaturationMin ? desaturationMin : value;
            Desaturation.SetFloat("_Intensity", value);

        }
    }

    public float DisplacementValue
    {
        set
        {
            value = value > DisplacementRange ? DisplacementRange : value;
            value = value < displacementMin ? displacementMin : value;
            Displacement.SetFloat("_Magnitude", value);
        }
    }

    private void Start()
    {
        DesaturationValue = 0;
        DisplacementValue = 0;
    }

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (DesaturationON && DisplacementON)
        {
            RenderTexture temp = RenderTexture.GetTemporary(src.width, src.height, 0, src.format);
            Graphics.Blit(src, temp, Desaturation);
            Graphics.Blit(temp, dst, Displacement);
            temp.Release();
            return;
        }
        if (DesaturationON && !DisplacementON)
        {
            Graphics.Blit(src, dst, Desaturation);
            return;
        }
        if (!DesaturationON && DisplacementON)
        {
            Graphics.Blit(src, dst, Displacement);
            return;
        }
        Graphics.Blit(src, dst);
    }
}
