﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Collectible", menuName = "Collectible")]
public class Collectible_Data : ScriptableObject {

    public new string name = "undefined";
    public string description = "undefined";
    public int price = 100;
    public Sprite image;

    [Range(-3, 3)] public float fearFactor = 0.0f;
    [Range(0, 5)] public float effectDelay = 2.0f;

}
