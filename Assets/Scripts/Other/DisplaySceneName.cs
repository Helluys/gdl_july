﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DisplaySceneName : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log(SceneManager.GetActiveScene().buildIndex);
        switch(SceneManager.GetActiveScene().buildIndex) {
            case 1 :
                gameObject.GetComponent<Text>().text = "Night one";
                break;
            case 2:
                gameObject.GetComponent<Text>().text = "Night two";
                break;
            case 3:
                gameObject.GetComponent<Text>().text = "Night three";
                break;
            default:
                gameObject.GetComponent<Text>().text = "";
                gameObject.SetActive(false);
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
