﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIManager : SingletonBehaviour<UIManager> {

    [Header("Fear")]
    [SerializeField] private Image fearBar;
    [Header("Message")]
    [SerializeField] private Text messageDisplay;
    [SerializeField] private float messageDelay = 2;
    private Coroutine cEraseMessage;

    public void UpdateFearBar (float value) {
        fearBar.fillAmount = value;
        fearBar.color = value * Color.red + (1 - value) * Color.white;
    }

    public void DisplayMessage(string message) {
        if (cEraseMessage != null)
            StopCoroutine(cEraseMessage);

        messageDisplay.text = message;
        cEraseMessage = StartCoroutine(EMessageErase());
    }

    private IEnumerator EMessageErase()
    {
        yield return new WaitForSeconds(messageDelay);
        messageDisplay.text = "";

    }
 
}
