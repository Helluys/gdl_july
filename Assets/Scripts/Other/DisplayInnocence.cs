﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayInnocence : MonoBehaviour {

    private Inventory inventory;
    private string iaDisplayed;

    void Start () {
        inventory = GameManager.GetInventory();
        iaDisplayed = inventory.innocenceAmount.ToString();
        DisplayIA();
    }

    void Update () {
        if (iaDisplayed != inventory.innocenceAmount.ToString()) {
            iaDisplayed = inventory.innocenceAmount.ToString();
            DisplayIA();
        }
    }

    private void DisplayIA () {
        gameObject.GetComponent<Text>().text = "Innocence : " + iaDisplayed;
    }
}
