﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public abstract class Controller : MonoBehaviour {

    public float acceleration = 5f;
    protected abstract Vector2 input { get; }

    public bool isDead { get; private set; }

    new protected Rigidbody2D rigidbody;
    private Animator animator;

    public enum Direction {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3
    }

    private Direction currentDirection;
    public bool running { get; private set; }

    public void Setup () {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    protected void OnUpdate () {
        UpdateAnimator();
    }

    protected void OnFixedUpdate () {
        if (isDead)
            return;

        if (input.magnitude > 0.1f) {
            rigidbody.AddForce(acceleration * input.normalized, ForceMode2D.Impulse);
            running = true;
        } else {
            running = false;
        }
    }

    private void UpdateAnimator () {
        currentDirection = ComputeDirection(input);
        animator.SetBool("Running", running);
        animator.SetInteger("Direction", (int) currentDirection);
    }

    private Direction ComputeDirection (Vector3 input) {
        Direction direction;

        if (input.magnitude < 0.2f)
            direction = currentDirection;
        else if (Mathf.Abs(input.x) > Mathf.Abs(input.y))
            direction = input.x > 0f ? Direction.RIGHT : Direction.LEFT;
        else
            direction = input.y > 0f ? Direction.UP : Direction.DOWN;

        return direction;
    }

    public void Die () {
        isDead = true;
        animator.SetTrigger("Death");
    }
}
