﻿using UnityEngine;

public class DepthPositioner : MonoBehaviour {

    private const float DEPTH_RATIO = 0.1f;

    private float baseDepth = 0f;
    
    void Start () {
        baseDepth = transform.position.z;
    }
    
    void Update () {
        Vector3 position = transform.position;
        position.z = baseDepth + transform.position.y * DEPTH_RATIO;
        transform.position = position;
    }
}
