﻿using UnityEngine;

[RequireComponent(typeof(Fear))]
[RequireComponent(typeof(SoundPlayer))]
public class PlayerController : Controller {

    [SerializeField] private Vector3 cameraOffset;

    private Interactable activeInteracable;

    private SoundPlayer soundPlayer;

    protected override Vector2 input {
        get {
            return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
    }

    private void Start () {
        // Call parent Controller callbacks
        Setup();

        soundPlayer = GetComponent<SoundPlayer>();

        Fear fear = GetComponent<Fear>();
        fear.DeathEvent += Die;
    }

    private void Update () {
        // Call parent Controller callbacks
        OnUpdate();

        if (running && !soundPlayer.isPlaying)
            soundPlayer.Play();
        else if (!running && soundPlayer.isPlaying)
            soundPlayer.Stop();

        if (Input.GetKeyDown(KeyCode.E) && activeInteracable)
            activeInteracable.Interact(gameObject);
    }

    private void FixedUpdate () {
        // Call parent Controller callbacks
        OnFixedUpdate();
    }

    private void LateUpdate () {
        Camera.main.transform.position = transform.position + cameraOffset;
    }

    private void OnTriggerStay2D (Collider2D other) {
        Interactable interactable = other.GetComponent<Interactable>();
        if (interactable) {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, (other.transform.position - transform.position));
            if (hit.collider.tag != "Interactable")
                return;
            if (activeInteracable) {
                if (DistanceTo(activeInteracable.gameObject) < DistanceTo(interactable.gameObject))
                    SetActiveInteractable(interactable);
            } else {
                SetActiveInteractable(interactable);
            }
        }
    }

    private void OnTriggerExit2D (Collider2D other) {
        Interactable interactable = other.GetComponent<Interactable>();
        if (interactable && interactable.Equals(activeInteracable))
            UnsetActiveInteractable();
    }

    private float DistanceTo (GameObject go) {
        return Vector2.Distance(transform.position, go.transform.position);
    }

    private void SetActiveInteractable (Interactable interactable) {
        if (activeInteracable)
            activeInteracable.HideInteraction();

        activeInteracable = interactable;
        activeInteracable.DisplayInteraction();

        if (activeInteracable.onSightMessage.Length > 0)
            UIManager.Instance.DisplayMessage(activeInteracable.onSightMessage);
    }

    private void UnsetActiveInteractable () {
        activeInteracable.HideInteraction();
        activeInteracable = null;
    }

}