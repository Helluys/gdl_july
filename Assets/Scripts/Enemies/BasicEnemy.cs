﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(PathFollowing))]
[RequireComponent(typeof(PlayerDetection))]
public abstract class BasicEnemy : MonoBehaviour {

    protected enum State {
        Wandering,
        Chasing
    }

    #region Variables

    #region Editor

    public float wanderingAcceleration = 1.5f;
    public float chasingAcceleration = 6;
    public List<Transform> wanderingPath;
    public List<float> wanderingStopTime;

    public float fearPower = 0.2f;

    #endregion

    #region Private

    protected PathFollowing pathFollowing;
    protected PlayerDetection playerDetection;
    protected State state = State.Wandering;
    protected bool isSeeingPlayer { get { return playerDetection.isInSight; } }

    protected Coroutine cWandering;
    protected Coroutine cChasing;

    #endregion

    #endregion

    #region Methods

    #region Unity

    void Start () {
        pathFollowing = GetComponent<PathFollowing>();
        playerDetection = GetComponent<PlayerDetection>();
        cWandering = StartCoroutine(Wandering(GetClosestPathPointIndex()));
        pathFollowing.acceleration = wanderingAcceleration;
    }

    protected virtual void Update () {
        StateTransition();

        if (isSeeingPlayer) {
            Fear.Instance.AddFearSource(gameObject, fearPower);
        } else
            Fear.Instance.RemoveFearSource(gameObject);
    }

    #endregion

    #region Private

    protected virtual void StateTransition () {

        if (isSeeingPlayer && state == State.Wandering) {
            state = State.Chasing;

            if (cWandering != null)
                StopCoroutine(cWandering);

            pathFollowing.acceleration = chasingAcceleration;
            cChasing = StartCoroutine(Chasing());
        }

        if (!isSeeingPlayer && state == State.Chasing) {
            state = State.Wandering;

            if (cChasing != null)
                StopCoroutine(cChasing);

            pathFollowing.acceleration = wanderingAcceleration;
            cWandering = StartCoroutine(Wandering(GetClosestPathPointIndex()));
        }
    }

    protected int GetClosestPathPointIndex () {
        float minDistance = Mathf.Infinity;
        int index = 0;
        for (int i = 0; i < wanderingPath.Count; i++) {
            float distance = Vector3.Distance(transform.position, wanderingPath[i].position);
            if (distance < minDistance) {
                minDistance = distance;
                index = i;
            }
        }
        return index;
    }

    protected IEnumerator Wandering (int initialPointIndex) {
        yield return new WaitUntil(() => pathFollowing.HasReachedEndOfPath());

        for (int i = initialPointIndex; i < wanderingPath.Count; i++) {
            pathFollowing.GoTo(wanderingPath[i].position);
            yield return new WaitUntil(() => pathFollowing.HasReachedEndOfPath());

            if (wanderingStopTime.Count > i && wanderingStopTime[i] > 0)
                yield return new WaitForSeconds(wanderingStopTime[i]);
        }

        cWandering = StartCoroutine(Wandering(0));
    }

    protected IEnumerator Chasing () {
        for (; ; ) {
            if (isSeeingPlayer)
                pathFollowing.GoTo(playerDetection.player.transform.position, true);
            else
                pathFollowing.GoTo(playerDetection.player.transform.position, false);
            yield return new WaitForSeconds(0.2f);
        }
    }

    #endregion

    #endregion
}
