﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : BasicEnemy{

    protected override void StateTransition()
    {
        if (isSeeingPlayer && state == State.Wandering)
        {
            state = State.Chasing;

            if (cWandering != null)
                StopCoroutine(cWandering);

            pathFollowing.acceleration = chasingAcceleration;
            cChasing = StartCoroutine(Chasing());
        }

        if (Hide.Instance.isHidden && state == State.Chasing)
        {
            state = State.Wandering;

            if (cChasing != null)
                StopCoroutine(cChasing);

            pathFollowing.acceleration = wanderingAcceleration;
            cWandering = StartCoroutine(Wandering(GetClosestPathPointIndex()));
        }
    }
}
