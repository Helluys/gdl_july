﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerDetection : MonoBehaviour {

    #region Variables

    public GameObject player;
    public float sightLength = 5;
    public float minDetectDistance = 1;
    public int fov = 90;
    public bool isInSight { get; private set; }
    public bool seeThroughWall = false;

    private Vector3 vecToPlayer { get { return (player.transform.position - transform.position).normalized; } }
    private RaycastHit2D raycastHit;

    new private Rigidbody2D rigidbody;

    #endregion

    #region Methods

    #region Unity

    private void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        player = GameManager.GetPlayer();
    }

    private void FixedUpdate () {
        SeePlayer();
    }

    #endregion

    #region Private

    private void SeePlayer () {

        if (Hide.Instance.isHidden)
        {
            isInSight = false;
            return;
        }

        isInSight = false;
        raycastHit = Physics2D.Raycast(transform.position, vecToPlayer, sightLength);
        if (raycastHit.collider != null) {
            if (raycastHit.collider.tag == "Player")
                isInSight = true;
        }
        if (seeThroughWall)
            isInSight = true;
        float distance = Vector2.Distance(transform.position, player.transform.position);

        if (distance < minDetectDistance)
            return;
        if (distance > this.sightLength)
            isInSight = false;
        if (Vector2.Angle(rigidbody.velocity.normalized, vecToPlayer) > fov / 2)
            isInSight = false;
    }

    #endregion

    #region Debug

    void OnDrawGizmos () {
        if (player == null)
            player = GameManager.GetPlayer();

        Vector2 direction = rigidbody ? rigidbody.velocity.normalized : (Vector2) transform.right;
        Vector3 vecLeftBound = Quaternion.Euler(0, 0, -this.fov / 2) * direction;
        Vector3 vecRightBound = Quaternion.Euler(0, 0, fov / 2) * direction;
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + vecLeftBound * sightLength);
        Gizmos.DrawLine(transform.position, transform.position + vecRightBound * sightLength);
        Gizmos.color = Color.grey;
        Gizmos.DrawWireSphere(transform.position, sightLength);


        Gizmos.color = isInSight ? Color.green : Color.red;

        if (raycastHit.collider != null)
            Gizmos.DrawLine(transform.position, raycastHit.point);
        else
            Gizmos.DrawLine(transform.position, transform.position + vecToPlayer * sightLength);
    }

    #endregion
    #endregion
}
