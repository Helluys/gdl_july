﻿using UnityEngine;

public class DetectionRangePositioner : MonoBehaviour {

    [SerializeField] new private Rigidbody2D rigidbody;
    
    // Update is called once per frame
    void Update () {
        if (rigidbody.velocity.magnitude > 0.2f)
            transform.rotation = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.right, rigidbody.velocity.normalized), Vector3.forward) * Quaternion.AngleAxis(45f, Vector3.forward);
    }
}
