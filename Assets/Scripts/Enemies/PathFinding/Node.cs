﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    public int gridX, gridY;
    public bool isObstacle;
    public Vector3 worldPos;
    public Node parentNode;
    public int gCost, hCost;
    public int FCost { get { return gCost + hCost; } }

    public Node(bool isObstacle, Vector3 worldPos, int gridX, int gridY)
    {
        this.isObstacle = isObstacle;
        this.worldPos = worldPos;
        this.gridX = gridX;
        this.gridY = gridY;
    }

}
