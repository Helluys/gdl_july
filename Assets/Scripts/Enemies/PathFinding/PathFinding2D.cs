﻿using System.Collections.Generic;

using UnityEngine;

public class PathFinding2D {

    public static int maxCellCheck = 1000;

    #region Public
    /// <summary>
    /// Gets the shortest path between two points
    /// </summary>
    /// <param name="startPos">from</param>
    /// <param name="targetPos">to</param>
    /// <returns>Shortest path between start and end</returns>
    public static List<Vector2> FindPath(Vector2 startPos, Vector2 targetPos)
    {
        //Need a grid
        if(Grid.Instance == null)
        {
            Debug.LogError("You need a grid to use the path finding");
            return null;
        }

        int cellCheckCount = 0;

        // Gets start and end nodes
        Node StartNode = Grid.Instance.NodeFromWorldPoint(startPos);
        Node TargetNode = Grid.Instance.NodeFromWorldPoint(targetPos);

        List<Node> OpenList = new List<Node>();
        List<Node> ClosedList = new List<Node>();
        OpenList.Add(StartNode);
        //While there's something in the open list
        while (OpenList.Count > 0 || cellCheckCount < maxCellCheck) {

            cellCheckCount++;
            //Sets the current node
            Node CurrentNode = OpenList[0];
            for (int i = 1; i < OpenList.Count; i++){
                if (OpenList[i].FCost < CurrentNode.FCost || OpenList[i].FCost == CurrentNode.FCost && OpenList[i].hCost < CurrentNode.hCost)
                    CurrentNode = OpenList[i];
            }

            //Update lists
            OpenList.Remove(CurrentNode);
            ClosedList.Add(CurrentNode);

            //If target has been reached
            if (CurrentNode == TargetNode)
            {
                return GetFinalPath(StartNode, TargetNode);
            }

            // Else continue A* algorithm
            // Loop through neighbors
            foreach (Node NeighborNode in Grid.Instance.GetNeighboringNodes(CurrentNode))
            {
                
                //Skip if obstacle or already visited
                if (NeighborNode.isObstacle|| ClosedList.Contains(NeighborNode))
                    continue;

                // Gets the cost of the neighbor node
                int MoveCost = CurrentNode.gCost + GetManhattanDistance(CurrentNode, NeighborNode);

                if (MoveCost < NeighborNode.gCost || !OpenList.Contains(NeighborNode))
                {
                    NeighborNode.gCost = MoveCost;
                    NeighborNode.hCost = GetManhattanDistance(NeighborNode, TargetNode);
                    NeighborNode.parentNode = CurrentNode;

                    if (!OpenList.Contains(NeighborNode))
                        OpenList.Add(NeighborNode);
                }
            }

        }
        //No path found
        return null;
    }

    #endregion

    #region Private

    /// <summary>
    /// Convert the shortest path into a list of vector2
    /// </summary>
    /// <param name="startNode"></param>
    /// <param name="endNode"></param>
    /// <returns></returns>
    private static List<Vector2> GetFinalPath(Node startNode, Node endNode)
    {
        List<Node> nodePath = new List<Node>();
        Node CurrentNode = endNode;

        while (CurrentNode != startNode)
        {
            nodePath.Add(CurrentNode);
            CurrentNode = CurrentNode.parentNode;
        }
       nodePath.Reverse();

       List<Vector2> finalPath = new List<Vector2>();
        for (int i = 0; i < nodePath.Count; i++) {
            finalPath.Add(new Vector2(nodePath[i].worldPos.x, nodePath[i].worldPos.y));
        }
        
            return finalPath;
    }

    /// <summary>
    /// Gets the Manhatten distance between two nodes
    /// </summary>
    /// <param name="nodeA"></param>
    /// <param name="nodeB"></param>
    /// <returns></returns>
    private static int GetManhattanDistance(Node nodeA, Node nodeB)
    {
        int ix = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int iy = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        return ix + iy;
    }

    #endregion
}
