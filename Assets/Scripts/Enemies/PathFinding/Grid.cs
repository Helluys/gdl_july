﻿using System.Collections.Generic;

using UnityEngine;

public class Grid : SingletonBehaviour<Grid> {
    #region Variables

    #region Editor
    [SerializeField]
    private LayerMask ObstacleMask;
    [SerializeField]
    private Vector2 gridWorldSize;
    [SerializeField]
    private float nodeRadius;
    #endregion

    #region Private
    private Node[,] nodeArray;
    private float nodeDiameter { get { return nodeRadius * 2; } }
    private Vector3 bottomLeft { get { return transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.up * gridWorldSize.y / 2; } }
    private int gridSizeX, gridSizeY;
    #endregion

    #endregion

    #region Methods
    #region Unity

    private void Start () {
        CreateGrid();
    }
    #endregion

    #region Private
    /// <summary>
    /// Creates the grid
    /// </summary>
    private void CreateGrid () {
        // Grid size
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        // Declare node array
        nodeArray = new Node[gridSizeX, gridSizeY];

        //Loops throught nodes
        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                //Get the world co ordinates of the current node
                Vector2 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);
                //Check for obstacle
                bool isObstacle = false;
                if (Physics2D.OverlapCircle(worldPoint, nodeRadius, ObstacleMask))
                    isObstacle = true;
                //Update array
                nodeArray[x, y] = new Node(isObstacle, worldPoint, x, y);
            }
        }
    }

    /// <summary>
    /// Updates the grid
    /// </summary>
    public void UpdateGrid () {
        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                Vector2 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);
                //Check for obstacle
                bool isObstacle = false;
                if (Physics2D.OverlapCircle(worldPoint, nodeRadius, ObstacleMask))
                    isObstacle = true;

                nodeArray[x, y].isObstacle = isObstacle;
            }
        }
    }

    /// <summary>
    /// Check if a position is inside the grid
    /// </summary>
    /// <param name="posX">x position </param>
    /// <param name="posY">y position</param>
    /// <returns>Whether it is inside the grid or not</returns>
    private bool IsInGrid (int posX, int posY) {
        if (posX >= 0 && posX < gridSizeX) {
            if (posY >= 0 && posY < gridSizeY)
                return true;
        }
        return false;
    }

    #endregion
    
    #region Public
    /// <summary>
    /// Gather all neighbors of a node
    /// </summary>
    /// <param name="neighborNode">node to get neighbors</param>
    /// <returns>List of neighbors</returns>
    public List<Node> GetNeighboringNodes (Node neighborNode) {
        List<Node> neighborList = new List<Node>();
        int checkX;
        int checkY;

        //Right
        checkX = neighborNode.gridX + 1;
        checkY = neighborNode.gridY;
        if (IsInGrid(checkX, checkY))
            neighborList.Add(nodeArray[checkX, checkY]);

        //Left
        checkX = neighborNode.gridX - 1;
        checkY = neighborNode.gridY;
        if (IsInGrid(checkX, checkY))
            neighborList.Add(nodeArray[checkX, checkY]);

        //Up
        checkX = neighborNode.gridX;
        checkY = neighborNode.gridY + 1;
        if (IsInGrid(checkX, checkY))
            neighborList.Add(nodeArray[checkX, checkY]);

        //Down
        checkX = neighborNode.gridX;
        checkY = neighborNode.gridY - 1;
        if (IsInGrid(checkX, checkY))
            neighborList.Add(nodeArray[checkX, checkY]);

        return neighborList;
    }
    /// <summary>
    /// Gets node from world position
    /// </summary>
    /// <param name="worldPos">world position</param>
    /// <returns>Closest node</returns>
    public Node NodeFromWorldPoint (Vector2 worldPos) {
        float xPos = ((worldPos.x + gridWorldSize.x / 2) / gridWorldSize.x);
        float yPos = ((worldPos.y + gridWorldSize.y / 2) / gridWorldSize.y);
        xPos = Mathf.Clamp01(xPos);
        yPos = Mathf.Clamp01(yPos);
        int x = Mathf.RoundToInt((gridSizeX - 1) * xPos);
        int y = Mathf.RoundToInt((gridSizeY - 1) * yPos);

        return nodeArray[x, y];
    }

    #endregion

    #region Debug

    private void OnDrawGizmosSelected () {
        //Boundaries
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, 1));

        if (nodeArray == null)
            return;

        //Draw nodes
        foreach (Node n in nodeArray) {
            if (n.isObstacle)
                Gizmos.color = Color.yellow;
            else
                Gizmos.color = Color.white;

            Gizmos.DrawCube(n.worldPos, Vector3.one * nodeDiameter);
        }
    }

    #endregion

    #endregion
}
