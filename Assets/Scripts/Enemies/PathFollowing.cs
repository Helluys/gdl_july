﻿using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PathFollowing : Controller {

    #region Variables

    [SerializeField] private float precision = 1;
    private int currentPathLength { get { return currentPath != null ? currentPath.Count : 0; } }
    private List<Vector2> currentPath;

    #endregion

    #region Methods

    #region Unity
    private void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        currentPath = new List<Vector2>();
        
        // Call parent Controller callbacks
        Setup();
    }

    private void Update () {
        Move();
        
        // Call parent Controller callbacks
        OnUpdate();
    }

    private void FixedUpdate () {
        // Call parent Controller callbacks
        OnFixedUpdate();
    }
    
    protected override Vector2 input {
        get {
            return  currentPath.Count > 0 ? Vector2.ClampMagnitude(currentPath[0] - (Vector2) transform.position, 1f) : Vector2.zero;
        }
    }

    #endregion

    #region Private

    /// <summary>
    /// Handles the movements
    /// </summary>
    private void Move () {
        if (currentPath == null || currentPath.Count == 0)
            Stop();
        else if (IsTargetReached(currentPath[0]))
                currentPath.RemoveAt(0);
    }

    private bool IsTargetReached (Vector2 target) {
        float d = Vector2.Distance(transform.position, target);
        if (d < this.precision)
            return true;
        return false;
    }

    #endregion

    #region Public

    public void Stop () {
        currentPath.Clear();
    }

    public void GoTo (Vector2 targetPosition, bool direct = false) {
        if (direct)
            currentPath = new List<Vector2>(new Vector2[] { targetPosition });
        else {
            List<Vector2> newPath = PathFinding2D.FindPath(transform.position, targetPosition);
            if (newPath != null)
                currentPath = newPath;
        }
    }

    public bool HasReachedEndOfPath() {
        return currentPathLength == 0;
    }

    #endregion

    #region Debug

    private void OnDrawGizmosSelected () {
        if (currentPath == null)
            return;

        Gizmos.color = Color.red;

        for (int i = 0; i < currentPath.Count; i++) {
            Gizmos.DrawSphere(new Vector3(currentPath[i].x, currentPath[i].y, 1), 0.2f);
        }
    }

    #endregion

    #endregion
}
