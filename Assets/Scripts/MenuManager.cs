﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    [Range(0,0.02f)]public float displacement;
    [Range(0, 1)] public float desaturation;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RenderingEffects.Instance.DisplacementValue = displacement;
        RenderingEffects.Instance.DesaturationValue = desaturation;
    }
}
