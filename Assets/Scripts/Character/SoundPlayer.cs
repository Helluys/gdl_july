﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SoundPlayer : MonoBehaviour {

    private AudioSource audioSource;

    [SerializeField] List<AudioClip> clips = new List<AudioClip>();
    [SerializeField] Vector2 pitchRange;
    [SerializeField] Vector2 volumeRange;
    [SerializeField] Vector2 delayRange;
    [SerializeField] bool autoPlay;
    [SerializeField] float spatialAmount = 1f;

    private Coroutine playCoroutine;

    public bool isPlaying { get { return playCoroutine != null; } }

    void Start () {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.spatialBlend = spatialAmount;
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        if (autoPlay)
            Play();
    }

    public void Play () {
        Stop();
        playCoroutine = StartCoroutine(PlayCoroutine());
    }

    public void Stop () {
        if (playCoroutine != null) {
            StopCoroutine(playCoroutine);
            playCoroutine = null;
        }
    }

    private IEnumerator PlayCoroutine () {
        for (; ; ) {
            yield return new WaitForSeconds(Random.Range(delayRange.x, delayRange.y));
            audioSource.clip = clips[Random.Range(0, clips.Count)];
            audioSource.pitch = Random.Range(pitchRange.x, pitchRange.y);
            audioSource.volume = Random.Range(volumeRange.x, volumeRange.y);
            audioSource.Play();
            yield return new WaitUntil(() => audioSource.isPlaying == false);
        }
    }

}
