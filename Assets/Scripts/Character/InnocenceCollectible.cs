﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnocenceCollectible : MonoBehaviour {

    [SerializeField] private int InnocenceValue = 1;

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Inventory.Instance.innocenceAmount += InnocenceValue;
            Destroy(this.gameObject);
        }
    }
}
