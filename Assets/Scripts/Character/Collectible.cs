﻿using UnityEngine;

public abstract class Collectible : MonoBehaviour
{

    //protected void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //        OnCollectibleTake();
    //}

    public virtual void OnCollectibleTake()
    {
        //Destroy(this.gameObject);
    }

}
