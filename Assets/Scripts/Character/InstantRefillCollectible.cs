﻿using UnityEngine;

public class InstantRefillCollectible : Collectible {

    [SerializeField][Range(-1,1)] private float RefillValue = 0;

    public override void OnCollectibleTake()
    {
        Fear.Instance.FearValue += RefillValue;
        base.OnCollectibleTake();
    }
}
