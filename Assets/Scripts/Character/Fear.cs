﻿using System.Collections.Generic;

using UnityEngine;

public class Fear : SingletonBehaviour<Fear> {

    [SerializeField] private float fearValue;
    public delegate void FearEventDel ();
    public FearEventDel DeathEvent;
    public FearEventDel WakeUpEvent;

    private Dictionary<GameObject, float> fearSources = new Dictionary<GameObject, float>();

    public bool isFearGrowing = false;
    [Range(0, 0.1f)] public float growingFearSpeed = 0.1f;

    private void Start () {
        //FearValue = 0.5f;
    }

    private void Update () {
        if (isFearGrowing)
            FearValue += growingFearSpeed * Time.deltaTime;
    }

    public float FearValue {
        get { return fearValue; }
        set {
            if (value > 1f) {
                value = 1f;
                if (DeathEvent != null)
                    DeathEvent();
            }

            if (value < 0f) {
                value = 0f;
                if (WakeUpEvent != null)
                    WakeUpEvent();
            }

            fearValue = value;
            UpdateRenderingEffects();
            UpdateUIFearBar();
        }
    }

    private void UpdateRenderingEffects () {
        if (RenderingEffects.Instance == null)
            return;
        RenderingEffects.Instance.DisplacementValue = RenderingEffects.Instance.DisplacementRange * FearValue;
        RenderingEffects.Instance.DesaturationValue = RenderingEffects.Instance.DesaturationRange * FearValue;
    }

    private void UpdateUIFearBar () {
        if (UIManager.Instance == null)
            return;
        UIManager.Instance.UpdateFearBar(FearValue);
    }

    public void AddFearSource (GameObject source, float fearPower) {
        if (!fearSources.ContainsKey(source)) {
            fearSources.Add(source, fearPower);
            growingFearSpeed += fearPower;
        }
    }

    public void RemoveFearSource (GameObject source) {
        if (fearSources.ContainsKey(source)) {
            growingFearSpeed -= fearSources[source];
            fearSources.Remove(source);
        }
    }
}
