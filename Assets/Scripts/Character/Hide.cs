﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : SingletonBehaviour<Hide> {

    public bool isHidden = false;
    private Collider2D[] colliders;
    private PlayerController playerController;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        colliders = GetComponents<Collider2D>();
        playerController = GetComponent<PlayerController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isHidden)
            HideAction();
    }

    public void HideAction() {

        isHidden = !isHidden;
        foreach (Collider2D coll in colliders)
            coll.enabled = !isHidden;
        spriteRenderer.enabled = !isHidden;
        playerController.enabled = !isHidden;

    }
}
