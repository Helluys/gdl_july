﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FearGrowthCollectible : Collectible {

    [SerializeField][Range(-3,3)] private float fearFactor = 0;
    [SerializeField] [Range(0, 5)] private float effectDelay = 2;

    public override void OnCollectibleTake()
    {
        Debug.Log("Ca marche ?");
        StartCoroutine(EOnCollectibleTake());
    }

    private IEnumerator EOnCollectibleTake()
    {
        //GetComponent<SpriteRenderer>().enabled = false;
        //GetComponent<BoxCollider2D>().enabled = false;

        float previousFearSpeed = Fear.Instance.growingFearSpeed;
        Fear.Instance.growingFearSpeed *= fearFactor;
        yield return new WaitForSeconds(effectDelay);
        Fear.Instance.growingFearSpeed = previousFearSpeed;
        base.OnCollectibleTake();
    }
}
