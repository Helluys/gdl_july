﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyBearTrigger : MonoBehaviour {

    private bool hasFallen = false;

    public void OnLibraryInteraction() {
        if(!hasFallen){
            hasFallen = true;
            gameObject.GetComponent<Animator>().SetTrigger("Fall");
        }
    }
}
