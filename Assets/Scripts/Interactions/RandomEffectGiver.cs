﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEffectGiver : MonoBehaviour {

    public string succesMessage;
    [Range(0, 1)] public float succesBonus = 0;
    public string failMessage;
    [Range(0, 1)] public float failMalus = 0;
    public int useNumber = 1;
    [Range(0, 1)] public float succesProbability = 0.5f;

    public void GetRandomEffect () {

        bool succes = Random.value < succesProbability;
        if (succes) {
            UIManager.Instance.DisplayMessage(succesMessage);
            Fear.Instance.FearValue -= succesBonus;
        } else {
            UIManager.Instance.DisplayMessage(failMessage);
            Fear.Instance.FearValue += failMalus;
        }
        useNumber--;
        if (useNumber == 0) {
            Destroy(GetComponent<Interactable>());
            Destroy(transform.GetChild(0).gameObject);
        }
    }
}
