﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class InteractionEvent : UnityEvent<GameObject> { }

public class Interactable : MonoBehaviour {

    public InteractionEvent OnInteraction;

    [SerializeField] private GameObject displayPrefab;
    [SerializeField] private Vector3 displayOffset;
    private GameObject display;

    public string onSightMessage = "";

    private void Start()
    {
        this.gameObject.tag = "Interactable";
    }

    public virtual void Interact (GameObject source) {
        OnInteraction.Invoke(source);
    }

    public void DisplayInteraction () {
        if (!display) {
            display = Instantiate(displayPrefab, transform.position + displayOffset, Quaternion.identity, transform);
            display.transform.localScale = Vector3.Scale(display.transform.localScale,
                new Vector3 (1f / transform.lossyScale.x, 1f / transform.lossyScale.y, 1f / transform.lossyScale.z));
        }

        display.SetActive(true);
    }

    public void HideInteraction () {
        if (display)
            display.SetActive(false);
    }
}