﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectibleDisplay : MonoBehaviour
{

    [SerializeField] private Collectible_Data item;
    [SerializeField] private Inventory inventory;

    [SerializeField] private Text nameText;
    [SerializeField] private Text descriptionText;
    [SerializeField] private Text priceText;

    [SerializeField] private Image illustration;
    [SerializeField] private int itemStock;
    [SerializeField] private Collectible collectible;

    private bool isAvailable = true;

    // Use this for initialization
    void Start()
    {
        inventory = GameManager.GetInventory();
        nameText.text = item.name + " : " + itemStock.ToString() + " available";
        descriptionText.text = item.description;
        priceText.text = item.price.ToString();

        illustration.sprite = item.image;

        itemStock = 3;
    }

    public void OnPurchase()
    {
        if (inventory.innocenceAmount >= item.price && isAvailable == true)
        {
            inventory.innocenceAmount -= item.price;
            collectible.OnCollectibleTake();
            itemStock -= 1;
            nameText.text = item.name + " : " + itemStock.ToString() + " available";
            if(itemStock<=0)
            {
                DisableCollectibleButton();
                nameText.text = item.name;
            }
        }
    }

    private void DisableCollectibleButton()
    {
        isAvailable = false;
        priceText.text = "";
        descriptionText.text = "Out of stock";
        Color _color = Color.white;
        _color.a = 0.5f;
        descriptionText.color = _color;
        nameText.color = _color;
        illustration.color = _color;
    }
}
