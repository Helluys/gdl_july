﻿using UnityEngine;

public class WinTrigger : MonoBehaviour {
    public void WinGame () {
        GameManager.Instance.WinGame();
    }
}
