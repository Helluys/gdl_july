﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bath : MonoBehaviour {

    [SerializeField] private Sprite emptyBath;
    [SerializeField] private GameObject teddyBear;
    [SerializeField] private Transform teddySpawn;

    public void EmptyBath() {
        GetComponent<Animator>().enabled = false;
        GetComponent<SpriteRenderer>().sprite = emptyBath;
        Destroy(GetComponent<Interactable>());
        Destroy(transform.GetChild(1).gameObject);
        Instantiate(teddyBear, teddySpawn);
    }
}
